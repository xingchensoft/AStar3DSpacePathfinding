﻿//#define ZH_CN
#define EN_US

namespace Hont.AStar
{
    public static class i18N
    {
#if EN_US
    public static readonly string TOTAL_SIZE = "Total Size";
    public static readonly string BUILD_SETTINGS = "Build Settings";
    public static readonly string ENABLE_FREE_HANDLE = "Enable Free Handle";
    public static readonly string INVERT = "Invert";
    public static readonly string NODES_TO_OBSTACLE = "Nodes To Obstacle";
    public static readonly string NODES_TO_WALKABLE = "Nodes To Walkable";
    public static readonly string MATCH_MESH = "Match Mesh";
    public static readonly string MATCH_COLLIDER = "Match Collider";
    public static readonly string INVERT_COLLISION = "Invert Collision";
    public static readonly string SUBDIVS = "Subdivs";
    public static readonly string MATCH_GRID_BOUNDS = "Match Grid Bounds";
    public static readonly string MATCH_FLOOR = "Match Floor";
    public static readonly string DEBUG_PATHFINDING = "Debug Pathfinding";
    public static readonly string CREATE_ASTAR_DATA = "Create AStar Data";
    public static readonly string SAVE_ASTAR_DATA = "Save AStar Data";
    public static readonly string LOAD_ASTAR_DATA = "Load AStar Data";
    public static readonly string CLEAR_HIER_DATA = "Clear Hierarchy Data";
    public static readonly string SEL_MAPPING_POINT = "Selection Mapping Point";
    public static readonly string EDIT_GROW = "Edit Grow";
    public static readonly string SEL_TO_ASTAR_ROOT = "Selected to Astar Root";
#endif

#if ZH_CN
        public static readonly string TOTAL_SIZE = "总网格";
        public static readonly string BUILD_SETTINGS = "构建设置";
        public static readonly string ENABLE_FREE_HANDLE = "开启自由控制";
        public static readonly string INVERT = "反转";
        public static readonly string NODES_TO_OBSTACLE = "全设为障碍物";
        public static readonly string NODES_TO_WALKABLE = "全设为可行走";
        public static readonly string MATCH_MESH = "匹配网格";
        public static readonly string MATCH_COLLIDER = "匹配碰撞框";
        public static readonly string INVERT_COLLISION = "反转";
        public static readonly string SUBDIVS = "细分";
        public static readonly string MATCH_GRID_BOUNDS = "匹配网格边界";
        public static readonly string MATCH_FLOOR = "匹配地面";
        public static readonly string DEBUG_PATHFINDING = "调试寻路";
        public static readonly string CREATE_ASTAR_DATA = "创建A*数据";
        public static readonly string SAVE_ASTAR_DATA = "保存A*数据";
        public static readonly string LOAD_ASTAR_DATA = "加载A*数据";
        public static readonly string CLEAR_HIER_DATA = "清空A*数据";
        public static readonly string SEL_MAPPING_POINT = "选中映射点";
        public static readonly string EDIT_GROW = "扩选";
        public static readonly string SEL_TO_ASTAR_ROOT = "选中主对象";
#endif
    }
}
