﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.AStar
{
    public class HontAStarUnityPathfindingDebuger : MonoBehaviour
    {
        public HontAStarUnity astar;
        public int count = 10;
        public float delay = 0.1f;


        void OnEnable()
        {
            if (astar == null) return;
            StartCoroutine(EnumeratorUpdate());
        }

        IEnumerator EnumeratorUpdate()
        {
            while (true)
            {
                for (int i = 0; i < count; i++)
                {
                    var aVector = default(Vector3);
                    var bVector = default(Vector3);

                    aVector.x = UnityEngine.Random.Range(astar.LocalBounds.min.x, astar.LocalBounds.max.x);
                    aVector.y = UnityEngine.Random.Range(astar.LocalBounds.min.y, astar.LocalBounds.max.y);
                    aVector.z = UnityEngine.Random.Range(astar.LocalBounds.min.z, astar.LocalBounds.max.z);

                    bVector.x = UnityEngine.Random.Range(astar.LocalBounds.min.x, astar.LocalBounds.max.x);
                    bVector.y = UnityEngine.Random.Range(astar.LocalBounds.min.y, astar.LocalBounds.max.y);
                    bVector.z = UnityEngine.Random.Range(astar.LocalBounds.min.z, astar.LocalBounds.max.z);

                    aVector = astar.transform.TransformPoint(aVector);
                    bVector = astar.transform.TransformPoint(bVector);

                    var pathArr = astar.StartPathfinding(aVector, bVector);

                    if (pathArr != null)
                        HontAStarUnityHelper.DebugPathfindingPath(pathArr);
                }

                yield return new WaitForSeconds(delay);
            }
        }
    }
}
