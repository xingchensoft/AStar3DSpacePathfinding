﻿using UnityEngine;
using System;
using System.Collections;

using Hont;

namespace Hont.AStar
{
    public class HontAStarUnityOCTTreeUserDataDebuger : MonoBehaviour
    {
        public HontAStarUnity astar;


        void OnEnable()
        {
            if (astar == null) return;

            astar.BuildOctTree();

            var items = astar.OctTree.Root.GetItems(node =>
            {
                if ((bool)node.GetUserData(HontAStarUnity.OCTTREEUSERDATA_HAS_WALKABLE_BOX, false) == false)
                {
                    return float.MinValue;
                }
                else
                {
                    return -Vector3.Distance(node.Bounds.center, transform.position);
                }
            });

            Array.Sort(items, (x, y) => (Vector3.Distance(x.Position, transform.position).CompareTo(Vector3.Distance(y.Position, transform.position))));

            foreach (var item in items)
            {
                if (!astar.Grid.GetIsWalkable(item.Value)) continue;

                transform.position = item.Position;
                break;
            }
        }
    }
}
