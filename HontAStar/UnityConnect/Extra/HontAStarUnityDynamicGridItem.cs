﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;

namespace Hont.AStar
{
    public class HontAStarUnityDynamicGridItem : MonoBehaviour
    {
        public HontAStarUnity astar;
        public bool isWalkable;
        public bool defaultWalkableState;
        Position? mOldPosition;


        void Update()
        {
            if (astar == null) return;
            if (astar.OctTree == null) return;

            if (mOldPosition.HasValue)
                astar.Grid.SetIsWalkable(mOldPosition.Value, defaultWalkableState);

            var pos = astar.transform.InverseTransformPoint(transform.position);

            var items = astar.OctTree.Root
                .GetItems(m => m.Bounds.Contains(pos) ? -Vector3.Distance(m.Bounds.center, pos) : float.MinValue);

            var target = items == null
                ? null
                : items.FirstOrDefault(m => new Bounds(m.Position, astar.MappingSize).Contains(pos));

            if (target != null)
            {
                astar.Grid.SetIsWalkable(target.Value, isWalkable);
                mOldPosition = target.Value;
            }
        }
    }
}
