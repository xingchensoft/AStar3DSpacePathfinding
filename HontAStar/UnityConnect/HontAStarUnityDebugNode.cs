﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;

namespace Hont.AStar
{
    public class HontAStarUnityDebugNode : MonoBehaviour
    {
        const float GIZMOS_BOX_ALPHA = 0.4f;

        [Serializable]
        public class DebugSetting
        {
            [HideInInspector]
            public bool isInPath;
            public bool isBeginNode;
            public bool isEndNode;
            [HideInInspector]
            public bool isDebuger;
            [HideInInspector]
            public Color debugerColor;
            [HideInInspector]
            public string debugerText;
        }

        public HontAStarUnity host;
        public bool isWalkable;
        public DebugSetting debugSetting = new DebugSetting();
        public int cost;
        public int mask;
        [HideInInspector]
        public int astar_x;
        [HideInInspector]
        public int astar_y;
        [HideInInspector]
        public int astar_z;
        public Transform customMappingTransform;
        public Vector3 OutputPosition { get { return transform.localPosition + customMappingTransform.localPosition; } }

        HontAStarUnity.SoloTypeEnum mSoloType;

        public Position AStarPosition { get { return new Position(astar_x, astar_y, astar_z); } }
        public Bounds Bounds { get { return new Bounds(transform.position, host.MappingSize); } }


        void OnDrawGizmos()
        {
            if (host == null) return;
            if (!host.advanceSetting.drawGizmos) return;
            if (host.soloDisplayType == HontAStarUnity.SoloTypeEnum.None) return;
            if (!debugSetting.isInPath && host.soloDisplayType == HontAStarUnity.SoloTypeEnum.Path) return;
            if (!isWalkable && host.soloDisplayType == HontAStarUnity.SoloTypeEnum.Walkable) return;
            if (isWalkable && host.soloDisplayType == HontAStarUnity.SoloTypeEnum.Obstacle) return;

            var oldMatrix = Gizmos.matrix;
            var oldColor = Gizmos.color;

            Gizmos.matrix = transform.localToWorldMatrix;

            if (host.soloDisplayType == HontAStarUnity.SoloTypeEnum.Mask)
            {
                UnityEngine.Random.seed = mask;
                Gizmos.color = new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), GIZMOS_BOX_ALPHA);
                UnityEngine.Random.seed = 0;
            }
            else if (host.soloDisplayType == HontAStarUnity.SoloTypeEnum.Cost)
            {
                UnityEngine.Random.seed = cost;
                Gizmos.color = new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), GIZMOS_BOX_ALPHA);
                UnityEngine.Random.seed = 0;
            }
            else
            {
                if (isWalkable) Gizmos.color = new Color(0, 0, 1, GIZMOS_BOX_ALPHA);
                else Gizmos.color = new Color(1, 0, 0, GIZMOS_BOX_ALPHA);
            }

            if (debugSetting.isInPath) Gizmos.color = new Color(0, 1, 0, GIZMOS_BOX_ALPHA);
            if (debugSetting.isBeginNode || debugSetting.isEndNode) Gizmos.color = new Color(1, 1, 0, GIZMOS_BOX_ALPHA);
            if (debugSetting.isDebuger) Gizmos.color = debugSetting.debugerColor;

            Gizmos.DrawWireCube(Vector3.zero, host.MappingSize);
            Gizmos.DrawCube(Vector3.zero, host.MappingSize);
            Gizmos.DrawSphere(customMappingTransform.localPosition, host.MappingSize.magnitude * 0.3f * 0.5f);

#if UNITY_EDITOR
            if (debugSetting.isDebuger)
                UnityEditor.Handles.Label(transform.localPosition, debugSetting.debugerText);
#endif

            Gizmos.color = oldColor;
            Gizmos.matrix = oldMatrix;
        }

        void OnDrawGizmosSelected()
        {
            if (host == null) return;
            if (!host.advanceSetting.drawGizmos) return;
            if (host.soloDisplayType == HontAStarUnity.SoloTypeEnum.None) return;
            if (!debugSetting.isInPath && host.soloDisplayType == HontAStarUnity.SoloTypeEnum.Path) return;
            if (!isWalkable && host.soloDisplayType == HontAStarUnity.SoloTypeEnum.Walkable) return;
            if (isWalkable && host.soloDisplayType == HontAStarUnity.SoloTypeEnum.Obstacle) return;

#if UNITY_EDITOR
            if (UnityEditor.Selection.gameObjects.Contains(gameObject))
            {
                if (UnityEditor.Tools.current != UnityEditor.Tool.None || UnityEditor.Tools.current == UnityEditor.Tool.View)
                    UnityEditor.Tools.current = UnityEditor.Tool.None;
            }
#endif

            var oldColor = Gizmos.color;
            var oldMatrix = Gizmos.matrix;

            Gizmos.matrix = transform.localToWorldMatrix;

            Gizmos.color = Color.white;
            Gizmos.DrawWireCube(Vector3.zero, host.MappingSize);

#if UNITY_EDITOR
            if (debugSetting.isDebuger)
                UnityEditor.Handles.Label(transform.position, debugSetting.debugerText);
            else
                UnityEditor.Handles.Label(transform.position, cost.ToString());
#endif
            Gizmos.color = oldColor;
            Gizmos.matrix = oldMatrix;
        }

        public void Init(HontAStarUnity host, Position astarPos)
        {
            this.host = host;
            this.cost = host.defaultCost;
            this.isWalkable = true;

            this.astar_x = astarPos.X;
            this.astar_y = astarPos.Y;
            this.astar_z = astarPos.Z;
        }
    }
}
