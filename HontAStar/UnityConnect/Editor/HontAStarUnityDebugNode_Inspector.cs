﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

namespace Hont.AStar
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(HontAStarUnityDebugNode))]
    public class HontAStarUnityDebugNode_Inspector : Editor
    {
        IHontAStarUnityMaskCustomEnum mMappingEnum;
        bool? mHasMappingEnum;
        bool mIsEditGrow;
        bool mIsEditGrowInit;

        Vector3 mMinLengthFreeHandle;
        Vector3 mMaxLengthFreeHandle;

        Vector3 mMinHeightFreeHandle;
        Vector3 mMaxHeightFreeHandle;

        Vector3 mMinWidthFreeHandle;
        Vector3 mMaxWidthFreeHandle;


        void OnSceneGUI()
        {
            if (!mIsEditGrow) return;

            var host = base.target as HontAStarUnityDebugNode;
            var bounds = default(Bounds);

            if (!mIsEditGrowInit)
            {
                bounds = host.Bounds;
                mMinLengthFreeHandle = bounds.center;
                mMaxLengthFreeHandle = bounds.center;
                mMinHeightFreeHandle = bounds.center;
                mMaxHeightFreeHandle = bounds.center;
                mMinWidthFreeHandle = bounds.center;
                mMaxWidthFreeHandle = bounds.center;

                mMinLengthFreeHandle.x = bounds.min.x;
                mMaxLengthFreeHandle.x = bounds.max.x;

                mMinHeightFreeHandle.y = bounds.min.y;
                mMaxHeightFreeHandle.y = bounds.max.y;

                mMinWidthFreeHandle.z = bounds.min.z;
                mMaxWidthFreeHandle.z = bounds.max.z;

                Debug.Log("Init");

                mIsEditGrowInit = true;
            }

            mMinLengthFreeHandle = Handles.FreeMoveHandle(mMinLengthFreeHandle, Quaternion.identity, HandleUtility.GetHandleSize(mMinLengthFreeHandle) * 0.05f, Vector3.zero, Handles.DotCap);
            mMaxLengthFreeHandle = Handles.FreeMoveHandle(mMaxLengthFreeHandle, Quaternion.identity, HandleUtility.GetHandleSize(mMaxLengthFreeHandle) * 0.05f, Vector3.zero, Handles.DotCap);

            mMinHeightFreeHandle = Handles.FreeMoveHandle(mMinHeightFreeHandle, Quaternion.identity, HandleUtility.GetHandleSize(mMinHeightFreeHandle) * 0.05f, Vector3.zero, Handles.DotCap);
            mMaxHeightFreeHandle = Handles.FreeMoveHandle(mMaxHeightFreeHandle, Quaternion.identity, HandleUtility.GetHandleSize(mMaxHeightFreeHandle) * 0.05f, Vector3.zero, Handles.DotCap);

            mMinWidthFreeHandle = Handles.FreeMoveHandle(mMinWidthFreeHandle, Quaternion.identity, HandleUtility.GetHandleSize(mMinWidthFreeHandle) * 0.05f, Vector3.zero, Handles.DotCap);
            mMaxWidthFreeHandle = Handles.FreeMoveHandle(mMaxWidthFreeHandle, Quaternion.identity, HandleUtility.GetHandleSize(mMaxWidthFreeHandle) * 0.05f, Vector3.zero, Handles.DotCap);

            bounds = host.Bounds;

            mMinLengthFreeHandle.y = bounds.center.y;
            mMinLengthFreeHandle.z = bounds.center.z;
            mMaxLengthFreeHandle.y = bounds.center.y;
            mMaxLengthFreeHandle.z = bounds.center.z;

            mMinHeightFreeHandle.x = bounds.center.x;
            mMinHeightFreeHandle.z = bounds.center.z;
            mMaxHeightFreeHandle.x = bounds.center.x;
            mMaxHeightFreeHandle.z = bounds.center.z;

            mMinWidthFreeHandle.x = bounds.center.x;
            mMinWidthFreeHandle.y = bounds.center.y;
            mMaxWidthFreeHandle.x = bounds.center.x;
            mMaxWidthFreeHandle.y = bounds.center.y;

            if (Event.current.type == EventType.MouseUp)
            {
                var min = new Vector3(mMinLengthFreeHandle.x, mMinHeightFreeHandle.y, mMinWidthFreeHandle.z);
                var max = new Vector3(mMaxLengthFreeHandle.x, mMaxHeightFreeHandle.y, mMaxWidthFreeHandle.z);

                var growBounds = new Bounds() { min = min, max = max };

                var childList = new List<HontAStarUnityDebugNode>();

                foreach (Transform item in host.host.transform)
                {
                    childList.Add(item.GetComponent<HontAStarUnityDebugNode>());
                }

                var changedBox = childList
                    .Where(m => m.isWalkable != host.isWalkable)
                    .Where(m => growBounds.Contains(m.Bounds.center));

                foreach (var item in changedBox)
                {
                    item.isWalkable = host.isWalkable;
                }
            }
        }

        public override void OnInspectorGUI()
        {
            if (base.targets.Length > 1 && GUILayout.Button(i18N.SEL_MAPPING_POINT))
            {
                var newSelectionList = new List<GameObject>();

                foreach (var item in targets)
                {
                    if (!(item is HontAStarUnityDebugNode)) continue;

                    var node = item as HontAStarUnityDebugNode;

                    newSelectionList.Add(node.customMappingTransform.gameObject);
                }

                Selection.objects = newSelectionList.ToArray();

                Tools.current = Tool.Move;
            }

            mIsEditGrow = GUILayout.Toggle(mIsEditGrow, i18N.EDIT_GROW, GUI.skin.button);

            if (!mIsEditGrow && mIsEditGrowInit)
            {
                SceneView.RepaintAll();
                mIsEditGrowInit = false;
            }
            else if (mIsEditGrow && !mIsEditGrowInit)
            {
                SceneView.RepaintAll();
            }

            if (GUILayout.Button(i18N.SEL_TO_ASTAR_ROOT))
            {
                var conv = base.target as HontAStarUnityDebugNode;
                Selection.activeGameObject = conv.host.gameObject;
            }

            base.OnInspectorGUI();

            if (base.targets.Length > 0)
            {
                var firstValue = base.targets[0];

                var host = firstValue as HontAStarUnityDebugNode;

                if (mHasMappingEnum == false) return;

                if (mMappingEnum == null && mHasMappingEnum.GetValueOrDefault(true))
                {
                    mMappingEnum = GetAssemblyInterfaceAndCreate<IHontAStarUnityMaskCustomEnum>();

                    if (mMappingEnum == null) mHasMappingEnum = false;
                }

                if (mMappingEnum != null)
                {
                    var type = Enum.ToObject(mMappingEnum.MaskEnum.GetType(), host.mask);

                    var targetValue = Convert.ToInt32(EditorGUILayout.EnumPopup(new GUIContent("Mask"), (Enum)type));

                    for (int i = 0; i < base.targets.Length; i++)
                    {
                        var item = base.targets[i] as HontAStarUnityDebugNode;

                        item.mask = targetValue;
                    }
                }
            }
        }

        TInterface GetAssemblyInterfaceAndCreate<TInterface>()
        {
            var ass = Assembly.GetAssembly(typeof(TInterface));

            var targetType = ass.GetTypes()
                .Where(m => m.GetInterface(typeof(TInterface).FullName) != null)
                .Where(m => !m.IsAbstract && !m.IsInterface).FirstOrDefault();

            if (targetType == null) return default(TInterface);

            var instance = (TInterface)ass.CreateInstance(targetType.FullName);

            return instance;
        }
    }
}
