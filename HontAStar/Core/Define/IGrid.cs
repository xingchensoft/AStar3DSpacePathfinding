﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.AStar
{
    interface IGrid
    {
        Node this[int x, int y, int z] { get; }
    }
}
