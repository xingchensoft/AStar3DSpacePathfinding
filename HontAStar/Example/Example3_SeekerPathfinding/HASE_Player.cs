﻿using UnityEngine;
using System.Collections;

namespace Hont.AStar
{
    public class HASE_Player : MonoBehaviour
    {
        public HontAStarUnitySeeker seeker;
        public Transform target;
        public float speed = 100f;

        bool mIsPathfinding;


        void Update()
        {
            if (seeker == null) return;
            if (target == null) return;
            if (mIsPathfinding) return;

            seeker.StartPathfinding(transform.position, target.position, (path) =>
            {
                if (path != null)
                    StartCoroutine(DoPathMove(path));
            });
        }

        IEnumerator DoPathMove(Vector3[] path)
        {
            Debug.Log("!!!");
            mIsPathfinding = true;
            yield return HontAStarUnityHelper.EasyPathMove(seeker.transform, path, speed, 6f);
            mIsPathfinding = false;
            yield return null;
        }
    }
}
